import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';
import './index.css';
import reportWebVitals from './reportWebVitals'; // Убедитесь, что вы импортировали reportWebVitals из правильного файла

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById('root')
);

// Вызовите reportWebVitals
reportWebVitals();
